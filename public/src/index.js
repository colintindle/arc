import { describeArc } from './helpers/arc'

const XMLNS = 'http://www.w3.org/2000/svg'

function arcParts(colours, startAngle = -135, endAngle = 135) {
  const segmentLength = (endAngle - startAngle) / colours.length

  // if (endAngle < startAngle) {
  //   colours = colours.reverse()
  //   [endAngle, startAngle] = [startAngle, endAngle]
  // }

  return colours.map((colour, ndx) => ({
    colour,
    start: startAngle + (ndx * segmentLength),
    end: startAngle + ((1 + ndx) * segmentLength)
  }))
}

// colour, width, d
function pathElement(attributes) {
  const path = document.createElementNS(XMLNS, 'path')
  attributes = Object.assign({
    'fill': 'none',
    'stroke': '#000',
    'stroke-width': '20',
  }, attributes)

  for (const attr in attributes) {
    path.setAttribute(attr, attributes[attr])
  }

  return path
}

function getArcs(parts, { cx = 100, cy = 100, cd = 90, thickness = 20 } = {}) {
  return parts.map(part => pathElement({
    'stroke': part.colour,
    'stroke-width': thickness,
    'd': describeArc(cx, cy, cd, part.start, part.end)
  }))
}


function coloureScaleArc(colours, size, thickness, startAngle, endAngle) {
  const parts = arcParts(colours, startAngle, endAngle)
  const arcs = getArcs(parts, { cx: size / 2, cy: size / 2, cd: (size / 2) - (thickness / 2), thickness })

  const svg = document.createElementNS(XMLNS, 'svg')
  svg.classList.add('arc')
  svg.setAttribute('viewBox', `0 0 ${size} ${size}`)

  for (const arc of arcs) {
    svg.appendChild(arc)
  }

  return svg
}

const root = document.querySelector('#root')
root.appendChild(coloureScaleArc(['red', 'orange', 'yellow', 'green', 'blue'], 2000, 400, -135, 135))
